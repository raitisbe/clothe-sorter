import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import './registerServiceWorker'
import router from './router'
import axios from 'axios'

Vue.config.productionTip = false
const baseURL = window.location.host.includes('localhost')
  ? 'http://localhost:3000/admin/'
  : 'http://drebes.raitisbe.id.lv/admin/'

axios.defaults.baseURL = baseURL
axios.defaults.withCredentials = true

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
