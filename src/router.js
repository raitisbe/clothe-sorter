import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Find from './views/Find.vue'
import Clothe from './views/Clothe.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/category/:category?/:location?', name: 'find', component: Find, props: true },
    { path: '/filter', name: 'filter', component: Find },
    { path: '/clothe/:id?', name: 'clothe', component: Clothe, props: true },
  ]
})
